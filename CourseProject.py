import re
"""the func print logo"""
def pirnt_logo():
    HANGMAN_ASCII_ART = "Welcome to the game Hangman\n _    _\n| |  | |\n| |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __\n|  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \\\n| |  | | (_| | | | | (_| | | | | | | (_| | | | |\n|_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|\n                     __/ |\n                    |___/\n"
    print(HANGMAN_ASCII_ART)

"""
The func gets:
1)A string called secret_word. The string represents the secret word the player has to guess.
2)A list called old_letters_guessed. The list contains the letters the player has guessed so far.
--------------------------------------------------
The function returns true if all the letters that make up the secret word are included in the list of letters the user guessed. Otherwise, the function returns False.
"""
def check_win (secret_word, old_letters_guessed):
    secret_word_len = len(secret_word)
    count = 0
    old_letters_guessed_len = len(old_letters_guessed)   
    for i in range(old_letters_guessed_len):
        if (old_letters_guessed[i] in secret_word):
            count+=1
    if(count == len(secret_word)):
        return True

"""The func gets 
A string called secret_word(The string represents the secret word the player has to guess)and a list called old_letters_guessed(The list contains the letters the player has guessed so far).
The function returns a string which consists of letters and lower hopes. The string displays the letters from the old_letters_guessed list that are in the secret_word string in their appropriate position, and the rest of the letters in the string (which the player has not yet guessed) as underlines."""
def show_hidden_word (secret_word, old_letters_guessed):
    secret_word_len = len(secret_word)
    old_letters_guessed_len = len(old_letters_guessed)
    hints = []
    for i in range (secret_word_len):
        hints.insert(i, "_")
    final_hints = ""
    for i in range (secret_word_len):
       
        for g in range (old_letters_guessed_len):
        
            if( secret_word[i] == old_letters_guessed[g]):
                hints[i]=secret_word[i]
                break
            elif ( secret_word[i] != old_letters_guessed[g]):
                hints[i] = "_"
    final_hints = " ".join(hints)

    return final_hints

"""
this function gets a strings letter_guessed (The string represents the character received from the player) and a list called old_letters_guessed(The list contains the letters the player has guessed so far).
The function returns a True of False and whether the user has guessed the character in the past.
"""
def check_valid_input (letter_guessed, old_letters_guessed,a):   
    letter_geussed_len = len(letter_guessed)
    old_letters_guessed_len = len(old_letters_guessed)
    letter_guessed = letter_guessed.lower()
    count_letters = 0
    count_chars_that_isnt_letters = 0
    for i in range(old_letters_guessed_len):
        if (letter_guessed == old_letters_guessed[i]):
            return False
    if (letter_guessed >= 'a' and letter_guessed <= 'z' and letter_geussed_len == 1 and letter_guessed in a):
        return True
    else:
        return False
"""
this function gets a strings letter_guessed (The string represents the character received from the player) and a list called old_letters_guessed(The list contains the letters the player has guessed so far).
The function returns a True of False and whether the user has guessed the character in the past.
"""

def try_update_letter_guessed (letter_guessed, old_letters_guessed):   
    letter_geussed_len = len(letter_guessed)
    old_letters_guessed_len = len(old_letters_guessed)
    letter_guessed = letter_guessed.lower()
    count_letters = 0
    count_chars_that_isnt_letters = 0
    for i in range(old_letters_guessed_len):
        if (letter_guessed == old_letters_guessed[i]):
            old_letters_guessed.sort()
            old_letters_guessed = " -> ".join(old_letters_guessed[::1])
           
            return False ,  old_letters_guessed
    if (letter_guessed >= 'a' and letter_guessed <= 'z' and letter_geussed_len == 1 ):
        old_letters_guessed.append(letter_guessed)
        return True, old_letters_guessed
    else:
        old_letters_guessed.sort()
        old_letters_guessed = " -> ".join(old_letters_guessed[::1]) 
        
        return False , old_letters_guessed


"""
The function gets as parameters:
1)A string (file_path) that represents a path to the text file.
2)An integer that represents the location of a particular word in a file.
-----------------------------------------------------------------------
The function returns a tapel consisting of two members in the following order:
1)The number of different words in a file, that is, does not include repetitive words.
2)A word in a position obtained as an argument to a function (index), which will be used as the secret word for guessing.
"""
def choose_word(file_path, index):
    new_list = []
    count = 1
    counting = 0
    list_no_repet = []
    return_staf = ()
    with open(file_path, 'r') as file:
        # reading each line    
        for line in file:
            # reading each word        
            for word in line.split():
                # displaying the words           
                new_list.insert(count, word)
                count += 1
    for i in range(len(new_list)):
        for j in range(len(new_list)):
            if (new_list[i] != new_list[j] and i != j ):
                list_no_repet.insert(counting, new_list[i])
    

    file = open(file_path, "rt")
    data = file.read()
    words = data.split()
    clean_list = []
    for i in words:
        if i not in clean_list:
            clean_list.append(i)
    lens = len(clean_list)

    while (index > len(new_list)):
        index -= len(new_list)
    return_staf += (lens, new_list[index-1])
    
    return  new_list[index-1]

def print_hangman(num_of_tries):
    tries = {1:"""x-------x""", 2:"""
    x-------x
    |
    |
    |
    |
    |""", 3:"""
    x-------x
    |       |
    |       0
    |
    |
    |
    """, 4:"""
    x-------x
    |       |
    |       0
    |       |
    |
    |
    """,5:"""
    x-------x
    |       |
    |       0
    |      /|\\
    |
    """,6:"""
    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |

    """,7:"""
    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |

    """} 
    return (tries[num_of_tries])

def main():
    old_letters_guessed = []
    i=1
    pirnt_logo()
    #calls the func print_logo
    path = input("Enter file path: ")
    index = int(input("Enter Index:"))
    choosen_word= choose_word(path, index)
    #calls the func choose_word
    choosen_word=str(choosen_word)
    if (i ==1):
        print(print_hangman(i))
		#calls the func print_hangman 

        print(show_hidden_word (choosen_word, old_letters_guessed))
		#calls the func show_hidden_word 
    while (i < 7):
        letter_guessed = input("Guess a letter: ")
        add_to_i = check_valid_input(letter_guessed,old_letters_guessed,choosen_word)
        #calls the func check_valid_input
        add_to_j ,printf = try_update_letter_guessed (letter_guessed, old_letters_guessed)
        #calls the func try_update_letter_guessed

        checkwin = check_win (choosen_word, old_letters_guessed)
        #calls the func check_win
        if(checkwin == True):
            print(" ".join(choosen_word))
            print("Win")
            exit()   
        if(add_to_j == False):
            print("X \n", printf, sep = "") 
        elif add_to_i == False:
            print(":(")
            i+=1
            print(print_hangman(i))
            #calls the func print_hangman

        print(show_hidden_word (choosen_word, old_letters_guessed))
        #calls the func show_hidden_word

    print("LOSE")
if __name__ == "__main__":
    main()