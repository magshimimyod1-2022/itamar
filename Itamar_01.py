import requests

# def step_a():

def extract_password_from_site():
    url = 'http://webisfun.cyber.org.il/nahman/files/file'
    url_end = '.nfo'
    pasword = ""

    for i in range(11,35):
        file_num = str(i)
    
        file_url = url + file_num + url_end
        file_as_txt = file_num + '.txt'
        r = requests.get(file_url, allow_redirects=True)

        open(file_as_txt, 'wb').write(r.content)
    for i in range (11, 35):
        file_number = str(i) + '.txt'
        f = open(file_number)
        f = f.read()
        pasword += f[99]
    return pasword
    # def main():
    #     print(extract_password_from_site())
    # if __name__ == '__main__':
    #     main()
# def step_b():
    
def find_most_common_words(file_path, number_of_words):
    counts = 0
    file_path = open(file_path, 'r')
    password = ''
    d = dict()
    for line in file_path:
        line = line.strip()
        line = line.lower()
        words = line.split(" ")
        for word in words:
            if word in d:
                d[word] = d[word] + 1
            else:
                d[word] = 1
    sort_orders = sorted(d.items(), key=lambda x: x[1], reverse = True)
    for i in sort_orders:
        if(counts != number_of_words):
            counts += 1
            password += i[0] + ' '
    file_path.close()
    return password
    # def main():
    
    #     file_path = 'words.txt'
    #     number_of_words = 6
    #     the_password = find_most_common_words(file_path, number_of_words)
    #     print(the_password)
        
    # if __name__ == "__main__":
    #     main()
def main():
    action = int(input("ENter action: "))
    if action == 1:
        print(extract_password_from_site())
    # print("\n-----------------------------------\n")

    elif action == 2:
        find_most_common_words("words.txt", 6)

if __name__ == "__main__":
    main()