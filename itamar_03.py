import socket
"""the func act as the server and return a movie to the client """
def server (msg):
    SERVER_ADDR = "54.71.128.194"
    SERVER_PORT = 92
    count = 0
    socketObject = socket.socket()
    socketObject.connect((SERVER_ADDR, SERVER_PORT))
    print("Connected to localhost")
    msg = str.encode(msg)
    socketObject.sendall(msg)
    while(True):
        if count == 0:
            movie = socketObject.recv(1024)
            movie = movie.decode()
        data = socketObject.recv(1024)
        if(data == b''):
            print("Connection closed\n")
            break
        count = count+1
    socketObject.close()
    
    to_cange = 'jpg"&id:'
    the_movie = movie.replace(to_cange,'.jpg"&id:')
    
    return the_movie
"""the func gets the data about the movie from the client and sends to the server"""
def client():

    port = 9091

    with socket.socket() as client_sock:
        client_sock.bind(("localhost", port))
        client_sock.listen(1)
        client_soc, client_address = client_sock.accept()
        with client_soc:
            msg2 = client_soc.recv(1024)
            msg2 = msg2.decode()
            if ("france" in msg2.lower()):
                eror ='ERROR#"France is banned!"'
                client_soc.sendall(eror.encode())
                return eror
            else :
                the_movie = server (msg2)
                #calls the server func and 
                client_soc.sendall(the_movie.encode())
    client_soc.close()

    return the_movie

def main():
    while True:
        client()
        #calls the client func 
if __name__ == "__main__":
    main()
    